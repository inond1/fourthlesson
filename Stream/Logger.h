#pragma once
#include "OutStream.h"
class Logger
{
private:
	static unsigned int _num;
	OutStream _outStream;
	FileStream _fileStream;
	bool _logToScreen;
public:
	Logger(const char *filename, bool logToScreen);
	~Logger();

	void print(const char *msg);
};
