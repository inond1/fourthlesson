#pragma warning(disable : 4996)
#include "OutStream.h"


OutStream::OutStream()
{
	this->_pFile =  stdout;
}

OutStream::~OutStream()
{
	fclose(_pFile);
}


OutStream& OutStream::operator<<(const char* str)
{
	fprintf(this->_pFile,"%s", str);
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(this->_pFile,"%d", num);
	return *this;
}

OutStream& OutStream::operator<<(const char*(*pf)())
{
	fprintf(this->_pFile, "%s", pf());
	return *this;
}


const char* endline()
{
	return "\n";
}

FileStream::FileStream(const char* fileName)
{
	this->_pFile = fopen(fileName, "w");
	if (!this->_pFile)
	{
		std::cerr << "[ERROR], file couldn't open\n";
	}
}

FileStream::~FileStream()
{
	fclose(this->_pFile);
}

//void FileStream::setFileStream(const char* fileName)
//{
//	if (this->_pFile) {
//		fclose(this->_pFile);
//	}
//	this->_pFile = fopen(fileName, "w");
//	if (!this->_pFile)
//	{
//		std::cerr << "[ERROR], file couldn't open\n";
//	}
//}

OutStreamEncrypted::OutStreamEncrypted() :_offset(AUTOMATIC_OFFSET)
{
	this->_pFile = stdout;

}



OutStreamEncrypted::OutStreamEncrypted(int& offset):_offset(offset)
{
	this->_pFile = stdout;
	
}

OutStreamEncrypted::~OutStreamEncrypted()
{
	fclose(this->_pFile);
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(const char* str)
{
	std::string sTr = str;
	for(int i = 0; i<sTr.size(); i++){
		if (sTr[i]<=ASCII_LIMIT && sTr[i]>ASCII_LIMIT-ASCII_TABLE_ROUND) {
			sTr[i] += this->_offset;
			if (sTr[i] > ASCII_LIMIT)
			{
				sTr[i] -= ASCII_TABLE_ROUND;
			}
		}
	}
	fprintf(this->_pFile, "%s", sTr.c_str());
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	std::string str = std::to_string(num);
	this->operator<<(str.c_str());
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(const char*(*pf)())
{
	const char* str = pf();
	this->operator<<(str);
	return *this;
}
