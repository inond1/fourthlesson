#pragma once
#include <fstream>
#include <stdio.h>
#include <iostream>
#include <string>
#define ASCII_LIMIT 126
#define ASCII_TABLE_ROUND 95
#define AUTOMATIC_OFFSET 3
class OutStream
{
protected:
	FILE* _pFile;
public:
	OutStream();
	~OutStream();

	OutStream& operator<<(const char* str);
	OutStream& operator<<(int num);
	OutStream& operator<<(const char*(*pf)());
};

const char* endline();

class FileStream: public OutStream
{
public:
	FileStream(const char* fileName);
	~FileStream();

};

class OutStreamEncrypted: public OutStream
{
private:
	int _offset;
public:
	OutStreamEncrypted();
	OutStreamEncrypted(int& offset);
	~OutStreamEncrypted();
	OutStreamEncrypted& operator<<(const char* str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(const char*(*pf)());
};
