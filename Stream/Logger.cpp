#include "Logger.h"

Logger::Logger(const char* filename, bool logToScreen):_outStream(OutStream()), _fileStream(FileStream(filename)), _logToScreen(logToScreen){}

unsigned int Logger::_num = 0;

Logger::~Logger()
{
	this->_fileStream.~FileStream();
	this->_outStream.~OutStream();
}

void Logger::print(const char* msg)
{
	this->_outStream << this->_num << endline;
	this->_fileStream << this->_num << endline;
	if (this->_logToScreen) {
		this->_outStream << msg << endline;
	}
	this->_fileStream << msg << endline;
	this->_num++;
}
